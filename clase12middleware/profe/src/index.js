const express = require("express");
const app = express();

function imprimirRequest(req, res, next) {
    console.log("Hora desde middleware:", Date.now());
    next();
}

function imprimirRequest2(req, res, next) {
    console.log("Path desde middleware2:", req.path);

    next();
}

app.use(imprimirRequest);
app.use(imprimirRequest2);
app.get('/ejemplo', (req, res) => {
    res.json("Respuesto ejemplo desde endpoint");
});


app.listen(3000, () => { console.log("Escuchando en el puerto 3000"); })