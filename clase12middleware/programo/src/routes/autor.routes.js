const express = require("express");
const router = express.Router();
const { obtenerAutores, crearAutor, obtenerAutor, eliminarAutor, editarAutor, obtenerLibros, agregarLibro, obtenerLibro, eliminarLibro, editarLibro } = require("../models/autor.models")
const { existenciaUsuario, existenciaLibro } = require("../middlewares/autor.middleware");

router.use(express.json());

//RUTA PRINCIPAL
router.get("/", (req, res) => {
    res.json(obtenerAutores());
});

router.post("/", (req, res) => {
    const usuarioNuevo = req.body;
    crearAutor(usuarioNuevo);
    res.status(201).send("Usuario añadido");
});


//AUTORES
router.get("/:id", existenciaUsuario, (req, res) => {
    const id = req.params.id;
    res.json(obtenerAutor(id));
});

router.delete("/:id", existenciaUsuario, (req, res) => {
    const id = req.params.id;
    eliminarAutor(id);
    res.send("Usuario eliminado")
});

router.put("/:id", existenciaUsuario, (req, res) => {
    const id = req.params.id;
    editarAutor(id);
    res.send("Usuario actualizado");
});

//LIBROS AUTORES

router.get("/:id/libros", existenciaUsuario, (req, res) => {
    const id = req.params.id;
    res.json(obtenerLibros(id));
});

router.post("/:id/libros", existenciaUsuario, (req, res) => {
    const id = req.params.id;
    const nuevoLibro = req.body;
    agregarLibro(id, nuevoLibro);
    res.status(201).send("Libro añadido");
});



router.get("/:id/libros/:idLibro", existenciaUsuario, existenciaLibro, (req, res) => {
    const id = req.params.id;
    const idLibro = req.params.idLibro;
    res.json(obtenerLibro(idLibro, id));
});

router.delete("/:id/libros/:idLibro", existenciaUsuario, existenciaLibro, (req, res) => {
    const id = req.params.id;
    const idLibro = req.params.idLibro;
    eliminarLibro(idLibro, id);
    res.send("Se elimino el libro");
});

router.put("/:id/libros/:idLibro", existenciaUsuario, existenciaLibro, (req, res) => {
    const id = req.params.id;
    const idLibro = req.params.idLibro;
    const nuevoLibro = req.body;
    editarLibro(idLibro, id, nuevoLibro);
    res.send("Libro actualizado")
})


module.exports = router;