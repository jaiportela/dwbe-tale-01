require("dotenv").config();
const express = require("express");
const compression = require("compression");
const app = express();

const PORT = process.env.PORT || 3000;
const env = process.env.NODE_ENV || "development";

const autorRoutes = require('./routes/autor.routes');
app.use('/autores', autorRoutes);


app.listen(PORT, () => {
    console.log("Listen to port: " + PORT);
})