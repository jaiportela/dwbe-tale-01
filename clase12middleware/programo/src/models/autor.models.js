const escritores = [{
    id: 1,
    nombre: "Jorge Luis",
    apellido: "Borges",
    fechaDeNacimiento: "24/08/1899",
    libros: [
        {
            id: 1,
            titulo: "Ficciones",
            descripcion: "Se trata de ...",
            anioPublicacion: 1944,
        },
        {
            id: 2,
            titulo: "El aleph",
            descripcion: "Se trata de ...",
            anioPublicacion: 1949,
        },
        {
            id: 3,
            titulo: "Otros",
            descripcion: "Se trata de ...",
            anioPublicacion: 1954,
        }
    ]
},
{
    id: 2,
    nombre: "Gabriel",
    apellido: "Garcia Marquez",
    fechaDeNacimiento: "01/01/1000",
    libros: [
        {
            id: 1,
            titulo: "Cien años de soledad",
            descripcion: "Se trata de ...",
            anioPublicacion: 1000,
        },
        {
            id: 2,
            titulo: "No se",
            descripcion: "Se trata de ...",
            anioPublicacion: 1000,
        },
        {
            id: 3,
            titulo: "Realismo Magico",
            descripcion: "Se trata de ...",
            anioPublicacion: 1000,
        }
    ]
},
];

const obtenerAutores = () => [...escritores];

const crearAutor = nuevoAutor => escritores.push(nuevoAutor);




function indexUsuario(id) {
    id = parseInt(id);
    return escritores.findIndex(escritor => escritor.id === id);
}



const obtenerAutor = id => escritores[indexUsuario(id)];

const editarAutor = (id, nuevoAutor) => { escritores[indexUsuario(id)] = nuevoAutor; }

const eliminarAutor = id => escritores.splice(indexUsuario(id), 1);


const obtenerLibros = id => obtenerAutor(id).libros;

const agregarLibro = (id, nuevoLibro) => { obtenerLibros(id).push(nuevoLibro); }

function indexLibro(id, idLibro) {
    idLibro = parseInt(idLibro);
    const libros = obtenerLibros(id);
    return libros.findIndex(libro => libro.id === idLibro);
}

const obtenerLibro = (idLibro, id) => { return obtenerLibros(id)[indexLibro(id, idLibro)]; }

const eliminarLibro = (idLibro, id) => {
    const iUsuario = indexUsuario(id);
    const iLibro = indexLibro(id, idLibro);
    escritores[iUsuario].libros.splice(iLibro, 1);
}

const editarLibro = (idLibro, id, nuevoLibro) => {
    const iUsuario = indexUsuario(id);
    const iLibro = indexLibro(id, idLibro);
    escritores[iUsuario].libros[iLibro] = nuevoLibro;
}



// const eliminarLibro = (idLibro, id) => { escritores[indexUsuario(id)].libros.splice(indexLibro, 1); }


module.exports = { indexUsuario, obtenerAutores, crearAutor, obtenerAutor, eliminarAutor, editarAutor, obtenerLibros, agregarLibro, indexLibro, obtenerLibro, eliminarLibro, editarLibro }