const express = require("express");
const app = express();
const { indexUsuario, indexLibro } = require("../models/autor.models");

function existenciaUsuario(req, res, next) {
    const id = req.params.id;
    if (indexUsuario(id) !== -1) next();
    else res.send("No se encontro el usuario");

}

function existenciaLibro(req, res, next) {
    const idLibro = req.params.idLibro;
    const id = req.params.id;
    if (indexLibro(id, idLibro) !== -1) next();
    else res.send("Libro no encontrado");

}

module.exports = {
    existenciaUsuario
    , existenciaLibro
};