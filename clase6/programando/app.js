const users = [['admin', 'admin'], ['user1', '1234'], ['test', 'abc123']];

const cuadrado = n1 => n1 * n1;
// console.log(cuadrado(5));


function validacionUsuario(user, password) {
    let valido;
    for (const data of users) {
        valido = (data[0] === user && data[1] === password) ? true : false;
        if (valido) break;
    }
    return valido;
}

document.getElementById("validar").addEventListener('click', () => {
    const usuario = document.getElementById('usuario').value;
    const password = document.getElementById('password').value;
    validacionUsuario(usuario, password) ? alert('Correcto') : alert('Usuario no encontrado');
});
