for (var i = 0; i < 10; i++) {
    console.log(i);
}

var esImpar = false;

while (!esImpar) {
    var ingreso = parseInt(prompt("Ingrese un numero par"));
    if (Number.isInteger(ingreso)) {
        if (ingreso % 2 === 1) {
            esImpar = true;
            console.log("Es par");
        }
    }
}

