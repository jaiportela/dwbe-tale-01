function mostrarLista(marcas) {
    document.getElementById("lista").innerHTML = "";
    for (const marca of marcas) {
        document.getElementById("lista").innerHTML += `<li>${marca}</li>`;
    }
}

let marcas = [];


document.getElementById("adicionar").addEventListener("click", () => {
    if (document.getElementById("marca").value !== "") {
        marcas.push(document.getElementById("marca").value);
        mostrarLista(marcas);
        document.getElementById("marca").value = "";
    }
});
document.getElementById("eliminarPrimero").addEventListener("click", () => {
    marcas.shift();
    mostrarLista(marcas);

});

document.getElementById("eliminarUltimo").addEventListener("click", () => {
    marcas.pop();
    mostrarLista(marcas);
});




