document.getElementById("sumar").addEventListener('click', (e) => {
    e.preventDefault();
    let numero1 = parseInt(document.getElementById("numero1").value);
    let numero2 = parseInt(document.getElementById("numero2").value);
    console.log(`El resultado de ${numero1} + ${numero2} es ${numero1 + numero2}`);
});
document.getElementById("restar").addEventListener('click', (e) => {
    let numero1 = parseInt(document.getElementById("numero1").value);
    let numero2 = parseInt(document.getElementById("numero2").value);
    console.log(`El resultado de ${numero1} - ${numero2} es ${numero1 - numero2}`);
});

document.getElementById("multiplicar").addEventListener('click', (e) => {
    let numero1 = parseInt(document.getElementById("numero1").value);
    let numero2 = parseInt(document.getElementById("numero2").value);
    console.log(`El resultado de ${numero1} * ${numero2} es ${numero1 * numero2}`);
});
document.getElementById("sumaDeCuadrados").addEventListener('click', (e) => {
    let numero1 = parseInt(document.getElementById("numero1").value);
    let numero2 = parseInt(document.getElementById("numero2").value);
    console.log(`El resultado de la suma de los cuadrados ${numero1}^2 + ${numero2}^2 es ${numero1 ** 2 + numero2 ** 2}`);
});