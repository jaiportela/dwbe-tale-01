// class Persona {
//     constructor(nombre, apellido, edad) {
//         this.nombre = nombre;
//         this.apellido = apellido;
//         this.edad = edad;
//     }

//     fullName() {
//         return `${this.nombre} ${this.apellido}`;
//     }

//     esMayor() {
//         return this.edad >= 18;
//     }
// }

// persona1 = new Persona('Jairo', 'Portela', 16);

// console.log(persona1.fullName());
// console.log(persona1.esMayor());

let persona2 = {};

persona2.nombre = 'Jairo';
persona2.apellido = "Portela";
persona2.edad = 22;
persona2.fullName = function () {
    return `${this.nombre} ${this.apellido}`;
};