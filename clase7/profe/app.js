// // FORMA MANUAL
// let carro = {
//     marca: "BMW",
//     modelo: "X5",
//     anio: 2018,
//     color: "rojo",
//     edad: function () {
//         return 2021 - this.anio;
//     }
// }

// console.log(carro);
// console.log(carro.edad());

// //Creacion de objetos con metodos
// function Usuario(nombre, apellido, email, usuario) {
//     this.usuario = usuario;
//     this.nombre = nombre;
//     this.apellido = apellido;
//     this.email = email;
// }

// let usuario1 = Usuario('Jairo', 'Portela', 'test@test.com', 'popertela');
// let usuario2 = Usuario('Popis', 'caquis', 'test@test.com', 'popertela');

//Creación de un objeto creando una clase
class Columna {
    constructor(base, altura, fc) {
        this.base = base;
        this.altura = altura;
        this.fc = fc;
    }
    areaBruta() {
        return this.base * this.altura;
    }
}

let columna1 = new Columna(400, 400, 28);

console.log(columna1);
columna1.base = 500;
console.log(columna1.base);
console.log(columna1.areaBruta());