const chalk = require("chalk");

const error = chalk.bold.red;
const valid = chalk.bold.bgKeyword("black").green;
const blancoYRojo = chalk.bgWhite.red;
const rojoYBlanco = chalk.bgRgb(255, 0, 0).white;


console.log(error("Errorrr!"));
console.log(chalk.bgCyan.rgb(4, 4, 256)("Hola mundo"));
console.log(valid("Valido"));
console.log(blancoYRojo("Santa Fe"));
console.log(rojoYBlanco("Santa Fesito || °)"));
console.log(blancoYRojo(
    `
 --------------------- 
|     MENSAJE MUY     |
|     IMPORTANTE      |
 --------------------- `
));

console.log(valid(
    ` ---------------------- 
|        VALIDO        |
 ---------------------- 
`
));

const env = require("./env.variables.json");
console.log(env);
console.log(process.env.NODE_ENV);

let node_env = process.env.NODE_ENV || "development";
console.log(node_env);
let variables = env[node_env];
console.log(variables);


console.log(valid(`El puerto de desarrollo es: ${variables.PORT}`));