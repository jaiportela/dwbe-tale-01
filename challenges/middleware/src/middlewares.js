const logger = (req, res, next) => {
    console.log(`request HTTP method: ${req.method}`);
    next();
}

const esAdministrador = (req, res, next) => {
    const usuarioAdministrador = false;
    if (usuarioAdministrador) {
        console.log('El usuario está correctamente logueado.');
        next();
    } else {
        res.send('No está logueado');
    }
}

const infoRequest = (req, res, next) => {
    console.log(`Información del request: ${req.path}`);
    next();
}

const logTime = (req, res, next) => {
    console.log("Time :" + Date.now());
    next();
}

module.exports = { logger, esAdministrador, infoRequest, logTime };