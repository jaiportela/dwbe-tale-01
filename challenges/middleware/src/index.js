require("dotenv").config();
const NODE_ENV = process.env.NODE_ENV || "Develpment";
const PORT = process.env.PORT || 3000;

const express = require('express');
const app = express();
const { logger, esAdministrador, infoRequest, logTime } = require('./middlewares');

app.use(express.json());
app.use(logger);
app.use(logTime);



app.get('/estudiantes', esAdministrador, (req, res) => {
    res.send([
        { id: 1, nombre: "Lucas", edad: 35 }
    ])
});

app.post('/estudiantes', (req, res) => {
    res.status(201).send();
});

app.get('/cursos/', infoRequest, (req, res) => {
    res.json([
        { id: 1, curso: "Backend" }, { id: 2, curso: "Frontend" }, { id: 3, curso: "Diseño UX/UI" }
    ]);
});

app.listen(PORT, () => console.log(`Listening on ${PORT}`));

