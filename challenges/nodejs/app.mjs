//¡Crea tu primer programa en NodeJS! Crea un array que contenga una lista de nombres y edades, pueden ser otros/as estudiantes de tu curso. Luego, imprime todo el listado con un console.log del array
import { estudiantes } from './names.mjs';


for (let i = 0; i < estudiantes[0].length; i++) {
    console.log(`${estudiantes[0][i]} tiene ${estudiantes[1][i]} años`);
}

// console.log(__dirname);
// setTimeout(() => {
//     console.log("Han pasado 2 segundos");
// }, 2000);
// console.log(__filename);
// console.log(require);
// console.log(module);
// console.log(process);