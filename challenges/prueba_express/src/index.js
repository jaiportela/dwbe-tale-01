
const express = require("express");
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const swaggerOptions = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Prueba de swagger',
            version: '0.0.1'
        }
    },
    apis: ['./src/index.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));


app.get('/saludo', function (req, res) {
    res.send({
        "product_id": 1234,
        "name": "Hola"
    });
});

/**
 * @swagger
 * /post:
 *  post:
 *    description: Prueba
 *    parameters:
 *    - name: product-id
 *      description: Id del producto
 *      in: formData
 *      required: true
 *      type: string
  *    - name: nombre
 *      description: nombre
 *      in: formData
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: apellido
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */
app.post('/post', (req, res) => {
    res.send('Bien hecho');
});

app.listen(3000, () => {
    console.log("Escuchando el puerto 3000!");
});