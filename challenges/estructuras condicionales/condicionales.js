document.getElementById("evaluar").addEventListener('click', (e) => {
    e.preventDefault();
    let numero1 = parseInt(document.getElementById("numero1").value);
    let numero2 = parseInt(document.getElementById("numero2").value);

    if (numero1 === numero2) {
        console.log(`${numero1} es igual a ${numero2}`);
    } else if (numero1 > numero2) {
        console.log(`${numero1} es mayor que ${numero2}`);
    } else {
        console.log(`${numero1} es menor que ${numero2}`)
    }

    switch (numero1) {
        case 1:
        case 2:
        case 3:
        case 4:
            console.log("Los primeros cuatro");
            break;
        case 5:
        default:
            console.log("Despues de 4");
            break;
    }

    numero1 >= 18 ? console.log("mayor de edad") : console.log("menor de edad");


});