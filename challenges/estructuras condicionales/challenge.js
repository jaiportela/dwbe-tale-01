let condicionUno = (1 == "1");
let condicionDos = (1 === "1");
let condicionTres = (0 || 1);
let condicionCuatro = (true && 1);
// let condicionCuatro = (1 && true);
let condicionCinco = (1 - 1) && (2 + 2);
let condicionSeis = (1 - 1) || (2 + 2);

let resultado = `Condicion Uno = ${condicionUno}
Condicion Dos = ${condicionDos}
Condicion Tres = ${condicionTres}
Condicion Cuatro = ${condicionCuatro}
Condicion Cinco = ${condicionCinco}
Condicion Seis = ${condicionSeis}
`;

console.log(resultado);
