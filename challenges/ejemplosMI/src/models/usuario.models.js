const personas = [];

function mostrarPersonas() {
    return personas;
}

function agregarPersonas(nuevoUsuario) {
    personas.push(nuevoUsuario);
}

function editarPersona(nombre, nuevoUsuario) {

    const posicion = personas.findIndex(usuario => usuario.nombre === nombre);
    console.log(posicion);
    if (posicion === -1) {
        return "No se encontro el usuario";
    }
    else {
        personas[posicion] = nuevoUsuario;
        return "Usuario actualizado";
    }
}

module.exports = { mostrarPersonas, agregarPersonas, editarPersona }