const express = require("express");
const { agregarPersonas, mostrarPersonas, editarPersona } = require("./models/usuario.models");
const app = express();



function condiccionesCorreo(req, res, next) {
    const { correo } = req.body;
    const personas = mostrarPersonas();
    if (personas.find(usuario => usuario.correo === correo)) {
        res.send("Ese correo ya esta en uso");
    } else if (correo === "") {
        res.send("El correo esta vacio");
    } else if (!correo.includes("@")) {
        res.send("falta el arroba en el correo");
    }
    else {
        next();
    }
}




app.use(express.json());




app.get('/prueba', (req, res) => {
    res.json(mostrarPersonas());
});


app.post('/prueba', condiccionesCorreo, (req, res) => {
    const usuario = req.body;
    agregarPersonas(usuario);

    res.send("Usuario añadido");

});

// app.delete('/prueba', (req, res) => {
//     personas.pop();
//     res.send("Ultimo usuario eliminado");
// });

app.put('/prueba', (req, res) => {
    const nuevaPersona = req.body;
    res.send(editarPersona("jairo", nuevaPersona));

});



const PORT = 3000;
app.listen(PORT, () => {
    console.log("Escuchando en el puerto 3000");
});

