document.getElementById("formulario").addEventListener("submit", (e) => {
    e.preventDefault();
    // console.log("Prueba de consola");
    let nombre = document.getElementById("nombre").value;
    let apellido = document.getElementById("apellido").value;
    let edad = parseInt(document.getElementById("edad").value);
    let licencia = document.getElementById("licencia").value;
    let fechaExpiracion = document.getElementById("expiracion").value;

    let resultado = `
    Nombre: ${nombre}
    Apellido: ${apellido}
    Edad: ${edad}
    Licencia: ${licencia}
    Fecha de expiracion: ${fechaExpiracion} `;


    let anio = parseInt(fechaExpiracion.substring(0, 4));
    let mes = parseInt(fechaExpiracion.substring(5, 7));
    let dia = parseInt(fechaExpiracion.substring(8, 10));
    fechaExpiracion=anio+mes/12+dia/365;
    fechaActual = new Date();
    anioActual = fechaActual.getFullYear();
    mesActual = fechaActual.getMonth() + 1;
    diaActual = fechaActual.getDate();
    fechaActual=anioActual+mesActual/12+diaActual/365;

    if (edad >= 18 && licencia === "si" && fechaExpiracion>fechaActual) {
        console.log(`${nombre} ${apellido} esta habilitado para conducir`);
    }else if(edad >= 18 && licencia === "si" && fechaExpiracion===fechaActual){
        console.log("Renueve su licencia");
    }
    else{
        console.log(`${nombre} ${apellido} no esta habilitado para conducir`);
    }
});