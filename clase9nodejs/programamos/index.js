const hobbies = ['Videojuegos', 'Peliculas', 'Leer', 'Escuchar musica', 'Cantar', 'Bailar', 'Correr'];
const fs = require('fs');

hobbies.forEach((hobbie, index) => {
    const texto = `Mi hobbie numero ${index + 1} es ${hobbie}\n`
    console.log(texto);
    fs.appendFileSync('listaHobbies.txt', texto, function (err) {
        if (err) console.log(err);
        else console.log("Saved!!");
    });
});
