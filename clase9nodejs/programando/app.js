const fs = require('fs');
const calculadora = require('./calculator');

class Operacion {
    constructor(num1, num2, operacion) {
        this.num1 = num1;
        this.num2 = num2;
        this.nombreOperacion = operacion;
    }
}

function anadirTexto(archivo, texto) {
    fs.appendFileSync(archivo, `${texto}\n`, (err) => {
        if (err) console.log(err);
        else console.log("Saved!");
    })
}


const nombreArchivo = 'calculos.txt';
let resultado = "";

anadirTexto(nombreArchivo, 'Programa iniciado');

// const operaciones = [[2, 4], [5, 2], [7, 6], [9, 8]];
const operaciones = [new Operacion(7, 9, "Sumar"), new Operacion(5, 8, "Multiplicar"), new Operacion(6, 1, "Restar"), new Operacion(5, 9, "Dividir")];

operaciones.forEach(operacion => {
    if (operacion.nombreOperacion === "Sumar") {
        resultado = calculadora.sumar(operacion.num1, operacion.num2);
    } else if (operacion.nombreOperacion === "Restar") {
        resultado = calculadora.restar(operacion.num1, operacion.num2);
    } else if (operacion.nombreOperacion === "Dividir") {
        resultado = calculadora.dividir(operacion.num1, operacion.num2);
    }
    else {
        resultado = calculadora.multiplicar(operacion.num1, operacion.num2);
    }
    anadirTexto(nombreArchivo, resultado);

});