// class Telefono {
//     constructor(marca,
//         gama,
//         modelo,
//         pantalla,
//         sistemaOperativo,
//         precio) {
//         this.marca = marca;
//         this.gama = gama;
//         this.modelo = modelo;
//         this.pantalla = pantalla;
//         this.sistemaOperativo = sistemaOperativo;
//         this.precio = precio;
//     };
// }

// class Telefonos {
//     constructor() {
//         this._telefonos = [];
//     }

//     obtenerTelefonos() {
//         return _telefonos;
//     }
//     obtenerMitadTelefonos() {
//         const mitad = Math.round(_telefonos.length / 2);
//         return _telefonos[0, mitad];
//     }
// }


// const telefonos = [
//     {
//         marca: "Samsung",
//         gama: "Alta",
//         modelo: "S11",
//         pantalla: "19:9",
//         sistemaOperativo: "Android",
//         precio: 1200,
//     }
// ];

// obte

// module.exports = Telefonos;

const telefonos = [
    {
        marca: "Samsung",
        gama: "Alta",
        modelo: "S11",
        pantalla: "19:9",
        sistema_operativo: "Android",
        precio: 1200
    },
    {
        marca: "Iphone",
        modelo: "12 pro",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "iOs",
        precio: 1500
    },
    {
        marca: "Xiaomi",
        modelo: "Note 10s",
        gama: "Media",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 300
    },
    {
        marca: "LG",
        modelo: "LG el que sea",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 800
    }
];

const obtenerTelefonos = () => {
    return telefonos;
}

const obtenerMitadTelefonos = () => {
    const mitad = Math.round(telefonos.length / 2);
    const mitadTelefonos = [];
    for (let index = 0; index < mitad; index++) {
        const telefono = telefonos[index];
        mitadTelefonos.push(telefono);
    }
    return mitadTelefonos;
}

const precioMayor = () => {
    let indexMayor = 0;
    let mayor = 0;
    telefonos.forEach((telefono, index) => {
        if (telefono.precio > mayor) {
            mayor = telefono.precio;
            indexMayor = index;
        }
    });
    return telefonos[indexMayor];
}

const precioMenor = () => {
    let indexMenor = 0;
    let menor = 1000000;
    telefonos.forEach((telefono, index) => {
        if (telefono.precio < menor) {
            menor = telefono.precio;
            indexMenor = index;
        }
    });
    return telefonos[indexMenor];
}




module.exports = { obtenerTelefonos, obtenerMitadTelefonos, precioMayor, precioMenor };
