const express = require('express');
const app = express();
const { obtenerTelefonos, obtenerMitadTelefonos, precioMayor, precioMenor } = require('./models/telefonos');

// app.get('/telefonos', (req, res) => {
//     res.json(obtenerTelefonos());
// });

app.get('/mitadtelefonos', (req, res) => {
    res.json(obtenerMitadTelefonos());
});

app.get('/telefonos', (req, res) => {
    const { precio } = req.query;
    if (precio === "max") res.json(precioMayor());
    else if (precio === "min") res.json(precioMenor());
    else res.json(obtenerTelefonos());

});

app.get('/mayorprecio', (req, res) => {
    res.json(precioMayor());
});

app.get('/menorprecio', (req, res) => {
    res.json(precioMenor());
});

app.listen(3000, () => {
    console.log('Escuchando en el puerto 3000');
});
