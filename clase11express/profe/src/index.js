require("dotenv").config();
const express = require("express");
const app = express();

app.use(express.json());

app.get('/', (req, res) => {
    res.send('Hola mundo');
});

const PORT = process.env.PORT || 3000;

const lista_usuarios = ["user1", "user2", "user3"];

//Para obtener usuarios
app.get('/usuarios', (req, res) => {
    res.json(lista_usuarios);
});

//Para crear usuarios
app.post('/usuarios', (req, res) => {
    const { nombre, apellido } = req.body;
    lista_usuarios.push(nombre + apellido);
    // lista_usuarios.push(req.body.nombre);
    res.send('Post a ruta usuarios');
});

//Para actualizar usuarios
app.put('/usuarios', (req, res) => {
    const { index } = req.query;
    const { nombre } = req.body;
    lista_usuarios[index] = nombre
    res.json("actualizado");
});

//Para eliminar usuarios
app.delete('/usuarios', (req, res) => {
    const { index } = req.query;
    lista_usuarios.splice(index, 1);
    res.json("Usuario eliminado");
})

app.listen(PORT, () => {
    console.log("Escuchando desde el puerto " + PORT);
});