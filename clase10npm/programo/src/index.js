const coolImages = require("cool-images");
const fs = require("fs");
const urlImagenes = coolImages.many(720, 1080, 10, false, true);
// console.log(urlImagenes);

urlImagenes.forEach((urlImagen, i) => {
    fs.appendFileSync("./src/url_imagenes.txt", `URL ${i + 1}: ${urlImagen}\n`, (err) => {
        if (err) console.log(err);
        else console.log("Success!!");
    });
    console.log("Url " + (i + 1) + ": " + urlImagen);
});